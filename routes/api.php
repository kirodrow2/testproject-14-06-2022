<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\Product\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => ['api'],
    'prefix' => '/v1/auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('register', [AuthController::class, 'register']);


    Route::group([
        'middleware' => ['jwt.auth']],
        function () {
            Route::get('me', [AuthController::class, 'me']);

            /* Cart */

        });


});


Route::group([
    'middleware' => ['jwt.auth', 'api'],
    'prefix' => '/v1'],

    function () {
        Route::resource('product', ProductController::class)->except(['show', 'index']);

    });


Route::group([
    'middleware' => ['api'],
    'prefix' => '/v1'],

    function () {
        Route::get('product/{product}', [ProductController::class, 'show']);
        Route::get('product/', [ProductController::class, 'index']);
        Route::get('full-price', [ProductController::class, 'getFullPrice']);


        Route::get('logs', [LogController::class, 'getLogs']);

    });

