<?php

return [

    'sales_url' => env('SALES_URL'),
    'sales_provider' => env('SALES_PROVIDER'),
    'sales_password' => env('SALES_PASSWORD'),

];
