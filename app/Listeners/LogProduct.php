<?php

namespace App\Listeners;

use App\Events\LoggerEvent;
use App\Models\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class LogProduct
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\LoggerEvent $event
     * @return void
     */
    public function handle(LoggerEvent $event)
    {
        $log = new Log();
        $log->user_id = Auth::id();
        $log->action = $event->status;
        $log->model = get_class($event->model);
        $log->model_id = $event->model->id;
        $log->save();
    }
}
