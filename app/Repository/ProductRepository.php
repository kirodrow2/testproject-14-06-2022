<?php

namespace App\Repository;


use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductRepository
{

    public function createProduct(array $data): Product
    {
        $product = new Product();
        $product->name = $data['name'];
        $product->category = $data['category'];
        $product->barcode = $data['barcode'];
        $product->price = $data['price'];
        $product->count = $data['count'];
        $product->created_by = Auth::id();
        $product->save();
        return $product;

    }

    public function updateProduct($productId, array $data): Product
    {
        $product = $this->getProductById($productId);
        $product->name = $data['name'];
        $product->category = $data['category'];
        $product->barcode = $data['barcode'];
        $product->price = $data['price'];
        $product->count = $data['count'];
        $product->updated_by = Auth::id();
        $product->save();
        return $product;
    }

    public function getProductById($productId): Product
    {
        return Product::findOrFail($productId);
    }

    public function getProductList(): Collection
    {
        return Product::all();
    }

    public function deleteProduct($productId)
    {
        $this->getProductById($productId)->delete();
    }

    public function getFullPrice()
    {
        $query = DB::select('select sum(products.price * products.count) as full_price from products');
        return $query[0]->full_price ?? 0;
    }

}
