<?php

namespace App\Http\Controllers;

use App\Http\Requests\Login;
use App\Http\Requests\UserSignUp;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'me', 'refresh']]);
    }


    private function username()
    {
        $login = request()->input('username');

        if (is_numeric(str_replace(['(', ')', '-', '+', ' '], '', trim($login)))) {
            $phone = str_replace(['(', ')', '-', '+', ' '], '', trim($login));
            \request()->merge([
                'phone' => $phone,
            ]);
        } else {
            \request()->merge([
                'email' => $login,
            ]);
        }
    }


    public function login(Login $request)
    {
        $this->username();

        $credentials = request(['email', 'password']);

        if ($token = auth()->attempt($credentials)) {
            return $this->respondWithToken($token);
        } else {
            if (User::where('phone', $request->username)->exists() || User::where('email', $request->username)->exists()) {
                return $this->sendError('The given data was invalid', 422, ['password' => 'incorrect password']);
            }

            return $this->sendError('The given data was invalid', 422, ['phone' => 'user not exist']);
        }
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }


    public function register(UserSignUp $request)
    {
        $data = $request->all();
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();
        $token = auth('api')->attempt(['email' => $request->email, 'password' => $request->password]);
        return $this->respondWithToken($token);
    }

    public function me()
    {
        dd(1231);
    }

}
