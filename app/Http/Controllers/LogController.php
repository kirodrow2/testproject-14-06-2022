<?php

namespace App\Http\Controllers;

use App\Events\LoggerEvent;
use App\Models\Log;

class LogController
{
    public function getLogs()
    {
        try {
            $logs = Log::paginate(15);
            return response()->json(['logs' => $logs], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }
}
