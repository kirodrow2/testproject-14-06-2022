<?php

namespace App\Http\Controllers\Product;

use App\Events\LoggerEvent;
use App\Http\Controllers\Controller;
use App\Repository\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function store(Request $request)
    {
        try {
            $product = $this->productRepository->createProduct($request->all());
            LoggerEvent::dispatch($product, 'create');
            return response()->json(['status' => 'product was creating', 'product' => $product], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }


    public function update($productId, Request $request)
    {
        try {
            $product = $this->productRepository->updateProduct($productId, $request->all());
            LoggerEvent::dispatch($product, 'update');
            return response()->json(['status' => 'product was updating', 'product' => $product]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'product does not exist'], 404);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }

    public function destroy($productId)
    {

        try {
            $this->productRepository->deleteProduct($productId);

            return response()->json(['status' => 'product was deleting']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'product does not exist'], 404);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }


    public function show($productId)
    {
        try {
            $product = $this->productRepository->getProductById($productId);
            return response()->json(['product' => $product]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'product does not exist'], 404);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }

    public function index()
    {
        try {
            $product = $this->productRepository->getProductList();
            return response()->json(['products' => $product]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'product does not exist'], 404);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }

    public function getFullPrice()
    {

        try {
            $price = $this->productRepository->getFullPrice();
            return response()->json(['price' => $price]);

        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'product does not exist'], 404);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'oops, something wrong'], 422);
        }
    }

}
